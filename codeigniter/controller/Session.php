<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session extends CI_Controller {

	/*** initial function	***/
	public function index()
	{
		if($this->session->userdata('id_profile')) // Verifica si existe una sessión activa.
		{
			if ($this->session->userdata('id_profile') === '1') // Admin.
			{

			}
			elseif ($this->session->userdata('id_profile') === '2') // Tecnico.
			{

			}
			elseif ($this->session->userdata('id_profile') === '2') // Usuario.
			{

			}
			else // Cualquier otro perfil fuera de los tres principales.
			{
				redirect(base_url('login/logout'));
			}
		}
		else // Si no existe ninguna session reenvia a la vista login.
		{
			redirect(base_url('login'));
		}
	}

	public function index1()
	{
		if ($this->session->userdata('id_profile') === '1'
			or $this->session->userdata('id_profile') === '2'
			or $this->session->userdata('id_profile') === '3')
		{
			// code
		}
		else // Si no existe ninguna de las sessiones anteriores (1, 2 o 3) se redirige a la vista login.
		{
			$this->session->sess_destroy();
			redirect(base_url('login/login_form'));
		}
	}

	public function compare_session()
	{
		if ($this->session->userdata('id_user') == $id_user)
		{
			$this->session->set_flashdata('msj_error', 'No puedes eliminar el usuario con el que estas logueado !!!');
			redirect(base_url('user'));
		}
		else
		{

		}

		if ($this->session->userdata('id_state') == '1')
		{
			$this->session->set_flashdata('msj_error', 'No puedes eliminar !!!');
			redirect(base_url('user'));
		}
		else
		{

		}
	}

}

/* End of file session.php */
/* Location: .//C/xampp/htdocs/code-help/codeigniter/controller/session.php */
