<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index()
	{

	}

	public function list() // Lista los usuarios
	{
		if($this->session->userdata('id_profile')) // Si existe una session activa carga la vista usuarios.
		{
			if ($this->session->userdata('id_profile') === '1') // Admin.
			{
				$data = array(
					'users'		=> $this->User_model->get_users(),
					'title' 	=> '-- Admin - User --',
					'bg_body' 	=> '',
					'header' 	=> 'layout/header',
					'content'	=> 'user/list',
					'footer' 	=> 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
			elseif ($this->session->userdata('id_profile') === '2') // Tecnico.
			{
				$data = array(
					'users'		=> $this->User_model->get_users(),
					'title' 	=> '-- Tecnico - User --',
					'bg_body' 	=> '',
					'header' 	=> 'layout/header',
					'content'	=> 'user/list',
					'footer' 	=> 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
			elseif ($this->session->userdata('id_profile') === '3') // Usuario.
			{
				$data = array(
					'users'		=> $this->User_model->get_users(),
					'title' 	=> '-- Tecnico - User --',
					'bg_body' 	=> '',
					'header' 	=> 'layout/header',
					'content'	=> 'user/list',
					'footer' 	=> 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
			else // Cualquier otro perfil fuera de los tres principales.
			{
				redirect(base_url('dashboard'));
			}
		}
		else // Si no existe ninguna session reenvia a la vista login.
		{
			redirect(base_url('login'));
		}
	}

	/* Create User */
	public function create_user()
	{
		$username 	= $this->input->post('username');
				$name 		= $this->input->post('name');
				$lastname 	= $this->input->post('lastname');
				$email 		= $this->input->post('email');
				$phone 		= $this->input->post('phone');
				$password 	= $this->input->post('password');

				// Validaciones del formulario con la librería form_validation
				$this->form_validation->set_rules('id_profile', 'Profile', 'trim|required|strip_tags|xss_clean',
					array(
						'required'	=> 'Este campo es requerido.',
					));
				$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 3 Caracteres.',
						'max_length'=> 'Maximo 12 Caracteres',
						'alpha'		=> 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 3 Caracteres.',
						'max_length'=> 'Maximo 12 Caracteres',
						'alpha'		=> 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('lastname', 'Apellido', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 3 Caracteres.',
						'max_length'=> 'Maximo 12 Caracteres',
						'alpha'		=> 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|max_length[40]|strip_tags|xss_clean|is_unique[users.email]',
					array(
						'required'	 	=> 'Este campo es requerido.',
						'valid_email'	=> 'Ingrese un correo valido.',
						'max_length'	=> 'Maximo 40 Caracteres',
						'is_unique' 	=> 'Este correo ya esta en uso.',
					));
				$this->form_validation->set_rules('phone', 'Telefono', 'trim|min_length[14]|max_length[14]|strip_tags|xss_clean|alpha_dash',
					array(
						'min_length'=> 'Minimo 14 Caracteres.',
						'max_length'=> 'Maximo 14 Caracteres',
						'alpha_dash'=> 'Introduzca solo numeros.',
					));
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 6 Caracteres.',
						'max_length'=> 'Maximo 12 Caracteres',
					)
				);
				$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[password]|strip_tags|xss_clean',
					array(
						'required'=> 'Este campo es requerido.',
						'matches' => 'Las contraseñas no coinciden.',
					)
				);
				if ($this->form_validation->run())
				{
					$data = array(
						'username' 	=> $username,
						'name' 		=> $name,
						'lastname' 	=> $lastname,
						'email' 	=> $email,
						'phone' 	=> $phone,
						'password' 	=> $password,
					);
					if ($this->User_model->add($user)) {
						$this->session->set_flashdata('msj_success', 'El usuario se guardo correctamente !!!');
						redirect(base_url('user'));
					}
					else{
						$this->session->set_flashdata('msj_error', 'No se guardo el usurio');
						redirect(base_url('user/add'));
					}
				}
				else
				{
					$this->add();
				}
	}

	public function delete($id_user)
	{
		$data = array(
			'user' 	=> $this->User_model->get_user($id_user),
		);

        if (isset($data['user']))
        {
        	$this->User_model->delete($id_user);
        	$this->session->set_flashdata('msj_success', 'El usuario se elimino correctamente !!!');
        	redirect(base_url('user'));
        }
		else
		{
			$this->session->set_flashdata('msj_error', 'El usuario no existe !!!');
			redirect(base_url('user'));
		}
	}
}

/* End of file User.php */
/* Location: .//C/xampp/htdocs/code-help/codeigniter/controller/User.php */
