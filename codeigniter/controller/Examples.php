<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examples extends CI_Controller {

	/* Load the view template view, and add the title, body and footer. */
	public function index()
	{
		$data ['title']		= "-- Title Page --";
		$data ['header']	= "view/header_view";
		$data ['content']	= "view/content_view";
		$data ['footer']	= "view/footer_view";
		$this->load->view('template_view', $data);
	}

}

/* End of file Examples.php */
/* Location: .//C/xampp/htdocs/code-help/codeigniter/controller/Examples.php */
