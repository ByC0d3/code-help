<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	/* Load profile User. */
	public function list_profile()
	{
		$this->db->select('*');
		$this->db->from('profile');
		$consult = $this->db->get();
		$result = $consult->result();
		return $result;
	}

	/* Create User.	*/
	public function create_user($user){
        if ($this->db->insert('users',$user))
		{
			$this->session->set_flashdata('msj-exito', 'El Usuario se registro correctamente !!!');
		}
		else
		{
			$this->session->set_flashdata('msj-error', 'Error al registrar el Usuario !!!');
		}
		redirect('user', 'refresh');
    }

    /* List User. */
    public function read_user(){
    	$this->db->select('*');
		$this->db->from('users');
		$this->db->join('profile', 'profile.id_profile = users.id_profile');
		$this->db->order_by('name','asc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

}

/* End of file User_model.php */
/* Location: .//C/xampp/htdocs/code-help/codeigniter/models/User_model.php */
