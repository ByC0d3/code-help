<label for="id_profile" class="control-label">Perfil:</label>
<select name="id_profile" class="form-control">
	<?php foreach($get_profiles as $value):?>
		<option value="<?php echo $value->id_profile; ?>" <?php echo $value->id_profile == $user->id_profile ? "selected":"";?>><?php echo $value->profile; ?></option>
	<?php endforeach;?>
</select>