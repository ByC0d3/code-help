<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Template for Codeigniter</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/...'); ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/...'); ?>"></script>
</head>
<body>
<?php $this->load->view($header);?>
<?php $this->load->view($cuerpo);?>
<?php $this->load->view($footer);?>
</body>
</html>
